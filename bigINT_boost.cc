#include <boost/multiprecision/cpp_int.hpp>
#include <iostream>

using namespace boost::multiprecision;

int main(int argc, char** argv){
    cpp_int rs = 2;
    for (auto i = 1; i<= 9999999; ++i)
        rs *= 2;
    // std::cout << rs << std::endl;
    std::cout << "number of digit of rs = " << rs.str().size() << "\n\n\n\n";
    // 3010300
}