#! /bin/bash
# compile
/usr/bin/g++ -fdiagnostics-color=always \
    -g /home/tz/Documents/big-number/sub_string_finder.cpp \
    -I /home/tz/Documents/big-number \
    -I /home/tz/Documents/big-number/3rd_party/eigen-3.4.0 \
    -I /home/tz/Documents/big-number/3rd_party/oneapi-tbb-2021.8.0/include \
    -L/usr/local/lib /home/tz/Documents/big-number/3rd_party/oneapi-tbb-2021.8.0/lib/intel64/gcc4.8/libtbb.so \
    -o /home/tz/Documents/big-number/bin/sub_string_finder
    

/usr/bin/g++ -fdiagnostics-color=always \
    -g /home/tz/Documents/big-number/sub_string_finder.cpp \
    -I /home/tz/Documents/big-number/3rd_party/eigen-3.4.0 \
    -I /home/tz/Documents/big-number/3rd_party/oneapi-tbb-2021.8.0/include \
    -L/usr/local/lib /home/tz/Documents/big-number/3rd_party/oneapi-tbb-2021.8.0/lib/intel64/gcc4.8/libtbb.so \
    -o /home/tz/Documents/big-number/bin/sub_string_finder

#! /bin/bash
# run 
export LD_LIBRARY_PATH=/home/tz/Documents/big-number/3rd_party/oneapi-tbb-2021.8.0/lib/intel64/gcc4.8:$LD_LIBRARY_PATH
/home/tz/Documents/big-number/bin/sub_string_finder
