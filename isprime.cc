#include <iostream>
#include <boost/multiprecision/cpp_int.hpp>

using namespace boost::multiprecision;

bool is_prime(cpp_int num){
    // implement check prime number here
    return true;
}

int main(int argc, char** argv){
    cpp_int num = 57;
    bool x = is_prime(num);
    
    if (x){
        std::cout << "number " << num << " is prime number\n";
    } else
        std::cout << "number " << num << " is factors number\n";
    return 0;
}